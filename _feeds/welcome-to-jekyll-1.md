---
layout: post
title: Martin Eden by Jack London
image: images/pic02.jpg
description: Stories from Martin Eden
date: 2020-05-20 12:00:00 +0700
categories: portfolio
---
[It was a hard summer for Martin](https://www.gutenberg.org/files/1056/1056-h/1056-h.htm).  Manuscript readers and editors were away on vacation, and publications that ordinarily returned a decision in three weeks now retained his manuscript for three months or more.  The consolation he drew from it was that a saving in postage was effected by the deadlock.  Only the robber-publications seemed to remain actively in business, and to them Martin disposed of all his early efforts, such as “Pearl-diving,” “The Sea as a Career,” “Turtle-catching,” and “The Northeast Trades.”  For these manuscripts he never received a penny.  It is true, after six months’ correspondence, he effected a compromise, whereby he received a safety razor for “Turtle-catching,” and that The Acropolis, having agreed to give him five dollars cash and five yearly subscriptions: for “The Northeast Trades,” fulfilled the second part of the agreement. \[...\]
