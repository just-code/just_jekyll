---
layout: post
title: Martin Eden continued
image: images/pic03.jpg
description: Even more stories from Martin Eden
date: 2020-05-20 12:00:00 +0700
categories: portfolio
---
But The Globe, a Chicago magazine, gave Martin the most cruel treatment of all.  He had refrained from offering his “Sea Lyrics” for publication, until driven to it by starvation.  After having been rejected by a dozen magazines, they had come to rest in The Globe office.  There were thirty poems in the collection, and he was to receive a dollar apiece for them.  The first month four were published, and he promptly received a cheek for four dollars; but when he looked over the magazine, he was appalled at the slaughter.  In some cases the titles had been altered: “Finis,” for instance, being changed to “The Finish,” and “The Song of the Outer Reef” to “The Song of the Coral Reef.”  In one case, an absolutely different title, a misappropriate title, was substituted.  In place of his own, “Medusa Lights,” the editor had printed, “The Backward Track.”  But the slaughter in the body of the poems was terrifying.  Martin groaned and sweated and thrust his hands through his hair.  Phrases, lines, and stanzas were cut out, interchanged, or juggled about in the most incomprehensible manner.  Sometimes lines and stanzas not his own were substituted for his.  He could not believe that a sane editor could be guilty of such maltreatment, and his favorite hypothesis was that his poems must have been doctored by the office boy or the stenographer.  Martin wrote immediately, begging the editor to cease publishing the lyrics and to return them to him.
