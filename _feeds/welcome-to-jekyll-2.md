---
layout: post
title: Martin Eden the writer
image: images/pic03.jpg
description: More stories from Martin Eden
date: 2020-05-20 12:00:00 +0700
categories: portfolio
---
For a sonnet on Stevenson he managed to wring two dollars out of a Boston editor who was running a magazine with a Matthew Arnold taste and a penny-dreadful purse.  “The Peri and the Pearl,” a clever skit of a poem of two hundred lines, just finished, white hot from his brain, won the heart of the editor of a San Francisco magazine published in the interest of a great railroad.  When the editor wrote, offering him payment in transportation, Martin wrote back to inquire if the transportation was transferable.  It was not, and so, being prevented from peddling it, he asked for the return of the poem.  Back it came, with the editor’s regrets, and Martin sent it to San Francisco again, this time to The Hornet, a pretentious monthly that had been fanned into a constellation of the first magnitude by the brilliant journalist who founded it.  But The Hornet’s light had begun to dim long before Martin was born.  The editor promised Martin fifteen dollars for the poem, but, when it was published, seemed to forget about it.  Several of his letters being ignored, Martin indicted an angry one which drew a reply.  It was written by a new editor, who coolly informed Martin that he declined to be held responsible for the old editor’s mistakes, and that he did not think much of “The Peri and the Pearl” anyway.