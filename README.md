# Just a Jekyll html5up demo

This folder just contains a jekyll demo for [html5up demo](https://html5up.net/). More paid templates exist at [pixelarity](https://pixelarity.com/).
Some of those themes exist as jekyll versions by [Andrew Banchich](https://github.com/andrewbanchich/phantom-jekyll-theme/)

[Here is a tutorial](https://jekyllrb.com/tutorials/convert-site-to-jekyll/) how to use any html template for jekyll.

## Installation

First configuration the local path for dependencies instead of use the global

`bundle config set path 'vendor/bundle'`

Install the dependencies for your project

`bundle install`

Start the development server

`bundle exec jekyll serve`

Browse your page at [http://localhost:4000](http://localhost:4000)

## Production build

Update correct configuration `_config.prod.yml`

Build the app by running the following command

`JEKYLL_ENV=production bundle exec jekyll build --config _config.yml,_config.prod.yml`



## Credits

Original README from HTML5 UP:

``` text
Photon by HTML5 UP
html5up.net | @ajlkn
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)


A simple (gradient-heavy) single pager that revisits a style I messed with on two
previous designs (Tessellate and Telephasic). Fully responsive, built on Sass,
and, as usual, loaded with an assortment of pre-styled elements. Have fun! :)

Demo images* courtesy of Unsplash, a radtastic collection of CC0 (public domain) images
you can use for pretty much whatever.

(* = Not included)

Feedback, bug reports, and comments are not only welcome, but strongly encouraged :)

AJ
aj@lkn.io | @ajlkn


Credits:

 Demo Images:
  Unsplash (unsplash.com)

 Icons:
  Font Awesome (fontawesome.io)

 Other:
  jQuery (jquery.com)
  Responsive Tools (github.com/ajlkn/responsive-tools)

```
